from odoo import models, fields, api, _
import os


class ExcelReportXlrd(models.Model):
    _inherit = 'excel.report.by.xlrd'

    @api.model
    def get_template_file(self):
        res = super(ExcelReportXlrd, self).get_template_file()
        file_path = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'static'))
        file_path += '/src/excel/PhieuXuatKho.xls'
        res.append((file_path, 'Phieu Xuat Kho'))
        return res

    @api.model
    def get_content_report(self):
        res = super(ExcelReportXlrd, self).get_content_report()
        res.append(('phieu_xuat_kho', 'Phiếu Xuất Kho'))
        return res

    @api.model
    def detail_content_report(self, content_template=False):
        if content_template == 'phieu_xuat_kho':
            content = [
                [1, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [2, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [3, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [4, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [5, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [6, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [7, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [8, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [9, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [10, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [11, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [12, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [13, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [14, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [15, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [16, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [17, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [18, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [19, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [20, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [21, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [22, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [23, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [24, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [25, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [26, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [1, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [2, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [3, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [4, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [5, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [6, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [7, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [8, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [9, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [10, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [11, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [12, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [13, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [14, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [15, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [16, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [17, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [18, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [19, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [20, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [21, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [22, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [23, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [24, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [25, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [26, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [1, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [2, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [3, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [4, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [5, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [6, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [7, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [8, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [9, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [10, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [11, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [12, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [13, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [14, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [15, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [16, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [17, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [18, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [19, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [20, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [21, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [22, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [23, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [24, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [25, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12],
                [26, 'Product in Phieu Xuat Kho', 'XLLSH', '1204/1220', 'Cai', 120, 12, 120 * 12]
            ]
            export_product = self.env['export.product.report'].sudo().search([
                ('report_id', '=', self.id)
            ])
            for line in export_product:
                content.append([len(content)+1, line.product_id.name or 'No name', line.block, line.bill_name,
                                line.unit_id, line.product_qty, line.price_unit, line.amount_total])
            return content
        return super(ExcelReportXlrd, self).detail_content_report(content_template)

    export_product_ids = fields.One2many('export.product.report', 'report_id')


class ExportProduct(models.Model):
    _name = 'export.product.report'

    report_id = fields.Many2one('excel.report.by.xlrd')
    product_id = fields.Many2one('product.product', 'Hang Hoa')
    block = fields.Char('Lo')
    bill_name = fields.Char('Don Hang')
    unit_id = fields.Char('Don vi tinh')
    product_qty = fields.Integer('So luong', default=1)
    price_unit = fields.Integer('Don gia')
    amount_total = fields.Integer('Thanh Tien')

    @api.onchange('product_qty', 'price_unit')
    def compute_amount_total(self):
        self.amount_total = self.product_qty*self.price_unit

    @api.onchange('product_id')
    def get_price_unit(self):
        self.price_unit = self.product_id.list_price