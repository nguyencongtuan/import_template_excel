from odoo import models, fields, api, _
from .xlrd_excel_report_template import ExcelReportXlrdTemplate


class XlrdExcelReportMain(ExcelReportXlrdTemplate):
    _inherit = 'report.excel_report.xlrd_excel_report_main'

    def get_style_cell(self, style_template=False):
        if style_template == 'combria_bold_green':
            return self.combria_bold_green
        return super(XlrdExcelReportMain, self).get_style_cell(style_template)
