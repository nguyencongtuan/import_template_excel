from odoo import models,fields, api, _


class ExcelReportXlrdTemplate(models.AbstractModel):
    _inherit = 'report.excel_report.xlrd_excel_report_template'

    def _define_format(self):
        self.combria_bold_green = self.wb.add_format({
            'font_name': 'Combria',
            'bold': True,
            'font_color': '#003300'
        })
        return super(ExcelReportXlrdTemplate, self)._define_format()
