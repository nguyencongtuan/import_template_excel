{
    'name': 'Phieu Xuat Kho template',
    'depends': ['base', 'excel_report', 'product'],
    'data': [
        'views/phieu_xuat_kho.xml',

        'static/src/data/xlrd_data_style.xml'
    ],
}